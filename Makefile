BINDIR := $(CURDIR)/bin
HELM_CHART_NAME := ingress-nginx
HELM_CHART_DIR := $(CURDIR)
HELM_VALUES_FILE := $(HELM_CHART_DIR)/values.yaml
HELM_NAMESPACE := default
SCRIPT_DIR := $(CURDIR)/scripts

.PHONY: all build clean

all: helm-template

helm-template:
	$Q helm template --namespace $(HELM_NAMESPACE) -f $(HELM_VALUES_FILE) $(HELM_CHART_NAME) $(HELM_CHART_DIR)

helm-diff:
	$Q helm diff upgrade --namespace $(HELM_NAMESPACE) -f $(HELM_VALUES_FILE) --install --context 0 $(HELM_CHART_NAME) $(HELM_CHART_DIR)

helm-upgrade: helm-diff
	$Q $(SCRIPT_DIR)/prompt_confirm.sh
	$Q helm upgrade --namespace $(HELM_NAMESPACE) -f $(HELM_VALUES_FILE) --install $(HELM_CHART_NAME) $(HELM_CHART_DIR)
